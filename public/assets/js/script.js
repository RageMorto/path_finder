$(document).ready(function () {
    let form = $('form');
    form.on('reset', function () {
        $('#result').html('');
    });

    form.on('submit', function (e) {
        e.preventDefault();
        let pointA = $('#point_a').val(),
            pointB = $('#point_b').val(),
            useNetworkXLib = $('#networkx').prop('checked')
        ;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                use_network_x: useNetworkXLib,
                point_a: pointA,
                point_b: pointB
            }),
            success: function (data) {
                if (data.min) {
                    renderPathInfo(data.min, data.response_time)
                } else {
                    renderErrorMessage('Path not found! :(')
                }
            }, error: function (error) {
                renderErrorMessage(error)
            }

        })
    })
})

function renderPathInfo(data, responseTime) {
    let template = '<div class="row">' +
        '<h3>Path found!</h3>' +
        '<p>Shortest path distance: <strong>' + data.distance + '</strong></p>' +
        '<p>Shortest path route: <strong>' + data.path.join(' - ',) + '</strong></p>' +
        '<p>Response time: ' + responseTime+ ' seconds</p>' +
        '</div>';
    $('#result').html(template)
}

function renderErrorMessage(errorMessage) {
    $('#result').html('<h3>' + errorMessage + '</h3>')
}