## Path finder
#### Installation
Unpack
```sh
$ tar -xzf path_finder.tgz
$ cd path_finder
```
or clone
```sh
$ git clone git clone git@bitbucket.org:RageMorto/path_finder.git
$ cd path_finder
```

Path to dataset file:
```sh
vim ./config.yaml
```
Install the dependencies and devDependencies and start the server.
```sh
$ virtualenv venv
$ source ./venv/bin/activate
$ pip install -r requirements.txt
$ python server.py
```

