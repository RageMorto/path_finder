import yaml
import collections
import json
import os.path as path
import networkx as nx


class CitiesGraph(object):

    def __init__(self, use_networkx=False):
        self.use_networkx = use_networkx
        config_path = yaml.safe_load(path.dirname(path.dirname(__file__)) + '/config.yaml')
        if path.isfile(config_path):
            with open(config_path, 'r') as file_stream:
                try:
                    self.config = yaml.safe_load(file_stream)
                    cities_source_file_path = self.config.get('cities_file')
                    if cities_source_file_path:
                        data = self.getJsonDataFromFile(cities_source_file_path)

                        if self.use_networkx:
                            self.graph = nx.DiGraph()
                            self.fill_nx_graph(data)
                        else:
                            self.graph = {}
                            self.fill_graph_dictionary(data)
                except yaml.YAMLError as e:
                    print(e)

    def fill_nx_graph(self, data):
        for city, directions in data.items():
            for direction_city, distance in directions.items():
                self.graph.add_edge(city, direction_city, distance=distance)

    def getJsonDataFromFile(self, file_path):
        with open(file_path, 'r') as file_stream:
            try:
                return collections.OrderedDict(sorted(json.load(file_stream).items()))
            except json.JSONDecodeError as e:
                print(e)
        return []

    # Returns an dictionary with shortest path distance and route
    def get_distance(self, point_a, point_b, use_network_x=False):
        if use_network_x:
            paths = self.get_shortest_path_by_network_X(point_a, point_b)
        else:
            paths = self.getDistances_from_graph(point_a, point_b)

        if not paths:
            return 'Route not found'
        distances = []
        for path_item in paths:
            distance = 0
            for index in range(len(path_item)):
                if len(path_item) == index + 1:
                    break
                distance += self.graph[path_item[index]][path_item[index + 1]]['distance']
            distances.append(distance)

        minimal_distance = min(distances)
        minimal_path = paths[distances.index(minimal_distance)]
        return {
            'min': {'distance': minimal_distance, 'path': minimal_path},
            'paths': paths,
            'distances': distances
        }

    def getDistances_from_graph(self, point_a, point_b, full_path=[]):
        full_path = full_path + [point_a]

        if point_a == point_b:
            return [full_path]

        if point_a not in self.graph:
            return []

        paths = []
        for destination in self.graph[point_a]:
            if destination not in full_path:
                new_paths = self.getDistances_from_graph(destination, point_b, full_path)
                for p in new_paths:
                    paths.append(p)

        return paths

    def fill_graph_dictionary(self, data):
        for city, directions in data.items():
            self.graph[city] = {direction_city: {'distance': distance} for (direction_city, distance) in
                                directions.items()}

    def get_shortest_path_by_network_X(self, point_a, point_b):
        try:
            return [nx.shortest_path(self.graph, point_a, point_b)]
        except nx.exception.NetworkXNoPath:
            return []
