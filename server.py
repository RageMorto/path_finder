import json
import time
from http.server import HTTPServer, BaseHTTPRequestHandler
from router.CitiesGraph import CitiesGraph
from os import curdir, sep


class Serv(BaseHTTPRequestHandler):

    def do_POST(self):
        start_time = time.time()
        if self.path == '/' and self.headers['content-type'] == 'application/json':
            length = int(self.headers['content-length'])
            stream_data = self.rfile.read(length)
            json_decoded = json.loads(stream_data)

            use_network_x = json_decoded['use_network_x']
            point_a = json_decoded['point_a']
            point_b = json_decoded['point_b']

            router = CitiesGraph(use_network_x)
            routes = router.get_distance(point_a, point_b, use_network_x)
            routes['response_time'] = time.time()-start_time

            response_body = json.dumps(routes)
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(bytes(response_body, 'utf-8'))
            return
        else:
            self.send_error(404, 'File Not Found: %s' % self.path)

    def do_GET(self):
        if self.path == '/':
            self.path = '/views/index.html'

            try:
                rendered = open(self.path[1:]).read()
                self.send_response(200)
            except:
                rendered = 'Not found'
                self.send_error(404)

            self.end_headers()
            self.wfile.write(bytes(rendered, 'utf-8'))
        else:
            try:

                send_reply = False
                if self.path.endswith(".html"):
                    mimetype = 'text/html'
                    send_reply = True
                if self.path.endswith(".jpg"):
                    mimetype = 'image/jpg'
                    send_reply = True
                if self.path.endswith(".gif"):
                    mimetype = 'image/gif'
                    send_reply = True
                if self.path.endswith(".js"):
                    mimetype = 'application/javascript'
                    send_reply = True
                if self.path.endswith(".css"):
                    mimetype = 'text/css'
                    send_reply = True
                if self.path.endswith(".svg"):
                    mimetype = 'image/svg+xml'
                    send_reply = True

                if send_reply:
                    # Open the static file requested and send it
                    print(curdir + self.path)
                    f = open(curdir + sep + self.path)
                    self.send_response(200)
                    self.send_header('Content-type', mimetype)
                    self.end_headers()
                    self.wfile.write(bytes(f.read(), 'utf-8'))
                    f.close()
                return

            except IOError:
                self.send_error(404, 'File Not Found: %s' % self.path)


print('starting server...')
port = 8080
server_address = 'localhost'
server_address_url = (server_address, port)
httpd = HTTPServer(server_address_url, Serv)
print(f'Serving HTTP on {server_address} port {port} (http://{server_address}:{port}/) ...')
httpd.serve_forever()
